$(".accordion").click(function() {
    var id = $(this).attr("id");
    $(`#${id} + .panel`).slideToggle();
})

$("#change-theme").click(function() {
    var theme = $("#theme");
    var to;
    if (theme.attr("href").indexOf("theme1") !== -1) to = '/static/theme2.css';
    else to = '/static/theme1.css';
    $("#theme").attr("href", to);
})

$(document).ready(function() {
    setTimeout(function() {
        $("#loading").css("display", "none");
        $("#main").removeClass("d-none");
    }, 1000);
})

function validateAll() {
    if (window.emailValid && window.nameValid && window.passwordValid) {
        $('#submit-subscribe').removeClass('disabled')
    } else {
        $('#submit-subscribe').addClass('disabled')
    }
}

function validateEmail() {
    var email = $('#id_email').val()
    $.post('/check-email/', {
        email: email
    }, function (response) {
        if (response.subscribed) {
            $('#id_email').addClass('is-invalid')
            $('.invalid-feedback.email').remove()
            $('#id_email').after(`<div class="invalid-feedback email">
                Email tersebut sudah pernah didaftarkan sebelumnya, silakan daftar dengan email lain.
            </div>`)
            window.emailValid = false
        } else if (!response.valid_email) {
            $('#id_email').addClass('is-invalid')
            $('.invalid-feedback.email').remove()
            $('#id_email').after(`<div class="invalid-feedback email">
                Masukkan alamat email yang valid.
            </div>`)
            window.emailValid = false
        } else {
            $('#id_email').removeClass('is-invalid')
            $('#id_email').addClass('is-valid')
            window.emailValid = true
        }
        validateAll()
    })
}

function validateName() {
    var name = $('#id_name').val()
    if (name.length > 0) window.nameValid = true
    else window.nameValid = false
    validateAll()
}

function validatePassword() {
    var name = $('#id_password').val()
    if (name.length > 0) window.passwordValid = true
    else window.passwordValid = false
    validateAll()
}

$('#id_email').keyup(validateEmail);
$('#id_name').keyup(validateName)
$('#id_password').keyup(validatePassword)

$('#submit-subscribe').click(function submitSubscribe(e) {
    e.preventDefault()
    var token = $('input[name=csrfmiddlewaretoken]').val()
    var email = $('#id_email').val()
    var name = $('#id_name').val()
    var password = $('#id_password').val()
    $.post('/subscribe/', {
        csrfmiddlewaretoken: token,
        email: email,
        name: name,
        password: password,
    }, function (response) {
        $('#subscribed-modal').modal()
    })
})

$('#subscribe-form').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
      e.preventDefault();
      return false;
    }
});
