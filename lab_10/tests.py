from django.test import TestCase, LiveServerTestCase
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from .models import Subscriber

class IndexUnitTest(TestCase):

    def test_get_success(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

class IndexFunctionalTest(LiveServerTestCase):

    def setUp(self):
        options = Options()
        options.add_argument('--dns-prefetch-disable')
        options.add_argument('--no-sandbox')
        options.add_argument('--headless')
        options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', options=options)
        super().setUp()

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_click_accordion(self):
        """
        assure first accordion from hidden to be displayed when clicked.
        """
        self.selenium.get(self.live_server_url)

        wait = WebDriverWait(self.selenium, 10)
        wait.until(EC.visibility_of_all_elements_located((By.ID, 'main')))

        accordion = self.selenium.find_element_by_id('acc-1')
        panel = self.selenium.find_element_by_css_selector('#acc-1 + .panel')
        display = panel.value_of_css_property('display')
        self.assertEqual(display, 'none')
        accordion.click()
        display = panel.value_of_css_property('display')
        self.assertEqual(display, 'block')

    def test_change_theme(self):
        """
        assure background color changed when the button clicked.
        """
        self.selenium.get(self.live_server_url)

        wait = WebDriverWait(self.selenium, 10)
        wait.until(EC.visibility_of_all_elements_located((By.ID, 'main')))

        button = self.selenium.find_element_by_id('change-theme')
        body = self.selenium.find_element_by_tag_name('body')
        background = body.value_of_css_property('background-color')
        theme1_color = 'rgba(255, 235, 153, 1)'
        self.assertEqual(background, theme1_color)
        button.click()
        background = body.value_of_css_property('background-color')
        theme2_color = 'rgba(255, 150, 140, 1)'
        self.assertEqual(background, theme2_color)

    def test_loading(self):
        """
        test loading spinner appears
        """
        self.selenium.get(self.live_server_url)
        loading = self.selenium.find_element_by_id('loading')
        display = loading.value_of_css_property('display')
        self.assertEqual(display, 'block')

class SubscribeUnitTest(TestCase):

    def test_get_success(self):
        response = self.client.get('/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_str_subscriber_is_email(self):
        sub = Subscriber.objects.create(email='faiz@ui.ac.id', name='faiz', password='faiz')
        self.assertEqual(sub.__str__(), 'faiz@ui.ac.id')

    def test_check_email_success(self):
        response = self.client.post('/check-email/', {'email': 'faiz@ui.ac.id'})
        data = response.json()
        self.assertFalse(data['subscribed'])

    def test_check_email_fail(self):
        Subscriber.objects.create(email='faiz@ui.ac.id', name='faiz', password='faiz')
        response = self.client.post('/check-email/', {'email': 'faiz@ui.ac.id'})
        data = response.json()
        self.assertTrue(data['subscribed'])

    def test_check_email_invalid(self):
        response = self.client.post('/check-email/', {'email': 'faizui.ac.id'})
        data = response.json()
        self.assertFalse(data['valid_email'])

    def test_invalid_request(self):
        response = self.client.post('/subscribe/', {'email': ''})
        self.assertEqual(response.status_code, 400)

    def test_subscribe(self):
        response = self.client.post('/subscribe/', {
            'email': 'faiz@ui.ac.id',
            'name': 'faiz',
            'password': 'faiz',
        })
        self.assertEqual(response.status_code, 201)

    def test_list_subscriber(self):
        Subscriber.objects.create(email='faiz@ui.ac.id', name='faiz', password='faiz')
        response = self.client.get('/subscriber-list/')
        data = response.json()
        self.assertListEqual(data['subscriber'], ['faiz@ui.ac.id'])

    def test_remove_subscriber_success(self):
        Subscriber.objects.create(email='faiz@ui.ac.id', name='faiz', password='faiz')
        response = self.client.post('/remove-subscriber/', {
            'email': 'faiz@ui.ac.id',
            'password': 'faiz',
        })
        data = response.json()
        self.assertEqual(data['deleted'], True)

    def test_remove_subscriber_fail(self):
        Subscriber.objects.create(email='faiz@ui.ac.id', name='faiz', password='faiz')
        response = self.client.post('/remove-subscriber/', {
            'email': 'faiz@ui.ac.id',
            'password': 'lala',
        })
        data = response.json()
        self.assertEqual(data['deleted'], False)
