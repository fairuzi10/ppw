from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .forms import SubscribeForm
from .models import Subscriber

def index(request):
    return render(request, 'index.html')

def subscribe(request):
    if request.method == 'POST':
        form = SubscribeForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse(status=201)
        else:
            return HttpResponse(status=400)
    else:
        form = SubscribeForm()
        return render(request, 'subscribe.html', {'form': form})

@csrf_exempt
def check_email(request):
    from django.core.validators import validate_email
    email = request.POST['email']
    num_user = Subscriber.objects.filter(email=email).count()
    try:
        validate_email(email)
        valid_email = True
    except:
        valid_email = False
    response = {
        'valid_email': valid_email,
        'subscribed': num_user == 1,
    }
    return JsonResponse(response)

def subscriber_list(request):
    subscribers = Subscriber.objects.all()
    subscribers_email = [subscriber.email for subscriber in subscribers]
    data = {
        'subscriber': subscribers_email
    }
    return JsonResponse(data)

@csrf_exempt
def remove_subscriber(request):
    email = request.POST['email']
    password = request.POST['password']
    subscriber = Subscriber.objects.filter(email=email, password=password)
    if subscriber.count() == 0:
        data = {
            'deleted': False,
        }
    else:
        subscriber.delete()
        data = {
            'deleted': True
        }
    return JsonResponse(data)
