from django.urls import path
from .views import index, subscribe, check_email, subscriber_list, remove_subscriber

app_name = 'lab_10'

urlpatterns = [
    path('', index, name='index'),
    path('subscribe/', subscribe, name='subscribe'),
    path('check-email/', check_email, name='check-email'),
    path('subscriber-list/', subscriber_list, name='subscriber-list'),
    path('remove-subscriber/', remove_subscriber, name='remove-subscriber'),
]
