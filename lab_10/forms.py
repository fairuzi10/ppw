from django.forms import ModelForm, PasswordInput
from .models import Subscriber

class SubscribeForm(ModelForm):
    class Meta:
        model = Subscriber
        fields = '__all__'
        widgets = {
            'password': PasswordInput(),
        }
